export const MESSAGE = {
    UNAUTHORIZED: 'Unauthorized',
};

export const AUC_REWARD_DEFAULT_PREFIX = 'аук';

export const ENDPOINTS = {
    CUSTOM_REWARDS: 'https://api.twitch.tv/helix/channel_points/custom_rewards',
    CUSTOM_REDEMPTIONS: 'https://api.twitch.tv/helix/channel_points/custom_rewards/redemptions',
    VALIDATE_TWITCH: 'https://id.twitch.tv/oauth2/validate',
    CAMILLE_BOT: 'https://camille-bot-beta.glitch.me',
}

export const REDEMPTION_STATUS = {
    CANCELED: 'CANCELED',
}

export enum RedemptionStatus {
    Canceled = 'CANCELED',
    Unfulfilled = 'UNFULFILLED',
    Fulfilled = 'FULFILLED',
}

export const TWITCH_TOPICS = {
    REDEMPTIONS: 'channel-points-channel-v1',
}

export const IS_PRODUCTION = process.env.NODE_ENV === 'production';
