const CONFIG = {
    PORT: process.env.PORT ?? 8000,
    JWT_SECRET: process.env.JWT_SECRET ?? '',
    MONGODB_ATLAS_CONNECTION_URL: process.env.MONGODB_ATLAS_CONNECTION_URL ?? '',
    TWITCH_CLIENT_ID: process.env.TWITCH_CLIENT_ID ?? '',
    TWITCH_CLIENT_SECRET: process.env.TWITCH_CLIENT_SECRET ?? '',
    TWITCH_REDIRECT_URL: 'http://localhost:3000/twitch/redirect',
    TWITCH_PUB_SUB_WS_URL: 'wss://pubsub-edge.twitch.tv',
    DONATIONALERTS_CLIENT_ID: process.env.DONATIONALERTS_CLIENT_ID ?? '',
    DONATIONALERTS_CLIENT_SECRET: process.env.DONATIONALERTS_CLIENT_SECRET ?? '',
    TELEGRAM_BOT_TOKEN: process.env.TELEGRAM_BOT_TOKEN ?? '',
    RANDOM_ORG_CLIENT_ID: process.env.RANDOM_ORG_CLIENT_ID ?? '',
};

export default CONFIG;
