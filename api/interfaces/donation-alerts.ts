export interface IDaUserData {
    id: number;
    code: string;
    name: string;
    avatar: string;
    email: string;
    socket_connection_token: string;
}

export interface DonationAlertsDonationsResponseDataInterface {
    id: number;
    name: string;
    username: string;
    message_type: string;
    message: string;
    amount: number;
    currency: string;
    is_shown: number;
    created_at: string;
    shown_at: string | null;
}