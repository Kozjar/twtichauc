export interface JwtTokenDataInterface {
    username: string;
    channelId: string;
}