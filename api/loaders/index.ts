import { Container } from 'typedi';
import { Application } from 'express';
import http from 'http';
import expressLoader from './express';
import mongooseLoader from './mongoose';
import websocketLoader from './websocket';
import User from '../models/user';
import TelegramBotService from '../services/telegram-bot';
import PlayerModel from '../models/playerModel';

export default async ({ app, server }: { app: Application, server: http.Server }) => {
    Container.set('userModel', User);
    Container.set('playerModel', PlayerModel);
    const telegramBotService = Container.get(TelegramBotService);
    await Promise.all([
        mongooseLoader(),
        websocketLoader({ app, server }),
        expressLoader({ app, server }),
        telegramBotService.connect(),
    ]);

    console.log('Complete loaders');
}