import config from '../config';
import mongoose from 'mongoose';

const mongooseLoader = async (): Promise<void> => {
    try {
        await mongoose.connect(config.MONGODB_ATLAS_CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: true });

        const connection = mongoose.connection;

        connection.on('error', (err) => {
            throw new Error(err);
        });
        connection.on('open', () => console.log('mongoose connected'));
    }
    catch(e) {
        console.error(e);
        throw e;
    }
};

export default mongooseLoader;
