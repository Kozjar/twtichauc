import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import config from '../config';
import { MESSAGE } from '../config/constants';
import { JwtTokenDataInterface } from '../interfaces/jwt-token-data';

const playerAuthMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  // let token = (req.headers['x-access-token'] || req.headers['authorization']) as string;
  const token: string = req.cookies.jwtToken;

  if (token) {
    // token = token.slice(7, token.length);

    jwt.verify(token, config.JWT_SECRET, (err, decoded) => {
      const jwtDecoded = decoded as JwtTokenDataInterface;

      if (err || !decoded) {
        return res.status(401).json({
          success: false,
          message: 'Token is invalid'
        });
      } else {
        res.locals.userData = {
          ...jwtDecoded,
        };
        next();
      }
    });
  }
  else {
    return res.status(401).send({
      success: false,
      message: MESSAGE.UNAUTHORIZED
    });
  }
}

export default playerAuthMiddleware;