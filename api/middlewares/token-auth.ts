import { Request, Response, NextFunction } from 'express';
import { MESSAGE } from '../config/constants';
import { verifyToken } from '../utils/token';

const tokenAuthMiddleware = async (req: Request, res: Response, next: NextFunction) => {
    // let token = (req.headers['x-access-token'] || req.headers['authorization']) as string;
    const token: string = req.cookies.jwtToken;
    const data = verifyToken(token);

    if (data) {
        res.locals.userData = { ...data };
        next();
    } else {
        return res.status(401).send({
            success: false,
            message: MESSAGE.UNAUTHORIZED
        });
    }
}

export default tokenAuthMiddleware;