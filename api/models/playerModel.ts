import mongoose from "mongoose";
import { ITwitchUser } from './user';

export interface PlayerProps extends ITwitchUser {
  skipRewardId: string,
  skipEmotes: {
    skip: {
      image: string,
      code: string,
    },
    safe: {
      image: string,
      code: string,
    }
  }
}

const Player = new mongoose.Schema(
  {
    username: String,
    channelId: String,
    twitchToken: {
      access_token: String,
      expires_in: Number,
      refresh_token: String,
      scope: [String],
      token_type: String,
    },
    daToken: {
      access_token: String,
      expires_in: Number,
      refresh_token: String,
      token_type: String,
    },
    skipRewardId: String,
    skipEmotes: {
      skip: {
        image: String,
        code: String,
      },
      safe: {
        image: String,
        code: String,
      }
    }
  },
);

const PlayerModel = mongoose.model<PlayerProps & mongoose.Document>('player', Player);

export default PlayerModel;
