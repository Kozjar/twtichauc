import { Router } from 'express';

const route = Router();

export default (app: Router) => {
  app.use('/', route);

  route.get('/isAlive', async (req, res, next) => {
    res.send();
    next();
  })
}
