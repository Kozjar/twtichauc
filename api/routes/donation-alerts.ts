import { Router } from 'express';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import { DonationAlertsAuthService } from '../services/donation-alerts-auth';
import UserService from '../services/user';
import jwt from 'jsonwebtoken';
import config from '../config';

const route = Router();

export default (app: Router) => {
    app.use('/donationalerts', route);

    const userService = Container.get(UserService);
    const donationAlertsAuthService = Container.get(DonationAlertsAuthService);

    route.post('/auth', async (req, res, next) => {
        try {
            console.log(req.cookies?.jwtToken);
            const userData = await donationAlertsAuthService.signIn(req.body.code, req.body.redirectUri, req.cookies?.jwtToken);
            console.log(userData);

            let token = jwt.sign(userData, config.JWT_SECRET, {
                expiresIn: 1000 * 60 * 60 * 24 * 30 * 12,
            });

            res.cookie('jwtToken', token);
            res.json({
                jwtToken: token,
            });

            next();
        }
        catch(e) {
            console.error(e);
            return next(new Error('Donation Alerts authentication failed'));
        }
    });

    route.get('/user', middlewares.TokenAuthMiddleware, async (req, res, next) => {
        const user = await userService.findUser({ username: res.locals.userData.username });
        const daUser = await donationAlertsAuthService.getUser(user!.donationAlerts!.token!.access_token);
        res.send(daUser);
        next();
    })
}