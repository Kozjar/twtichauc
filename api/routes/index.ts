import { Router } from 'express';
import settings from './settings';
import twitch from './twitch';
import user from './user';
import donationAlerts from './donation-alerts';
import common from './common';
import player from './player';
import random from './random';
import requests from './requests';

export default () => {
    const app = Router();

    user(app);
    settings(app);
    twitch(app);
    donationAlerts(app);
    common(app);
    player(app);
    random(app);
    requests(app);

    return app;
}