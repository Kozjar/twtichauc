import {
  NextFunction,
  Request,
  Response,
  Router
} from 'express';
import jwt from 'jsonwebtoken';
import { Container } from 'typedi';
import config from '../config';
import { PlayerAuthService } from '../services/twitch-auth';
import TwitchRedemptionService, { getCustomRewardsConfig } from '../services/twitch-pub-sub/twitch-redemption-service';
import middlewares from '../middlewares';
import { RedemptionStatus } from '../config/constants';
import PlayerModel from '../models/playerModel';
import WebsocketServer from '../websocket/websocket-server';
import { RESPOND_MESSAGE_TYPES } from '../websocket/respond-message-types';
import ExtensionCredentials from '../services/ExtensionCredentials';

const route = Router();

export default (app: Router) => {
  app.use('/player', route);
  const twitchRedemptionService = Container.get(TwitchRedemptionService);
  const twitchAuthService = Container.get(PlayerAuthService);
  const extensionCredentials = Container.get(ExtensionCredentials);
  const websocketServer = Container.get(WebsocketServer);

  route.get('/userData', middlewares.TokenAuthMiddleware, async (req, res, next) => {
    const { channelId } = res.locals.userData;
    const user = await PlayerModel.findOne({ channelId }).exec();

    if (user && user.twitchToken?.access_token) {
      let { channelId, twitchToken: { access_token }, username, skipRewardId } = user;
      const isValidTwitchToken = await twitchAuthService.validateToken({ channelId });

      if (!isValidTwitchToken) {
        access_token = (await twitchAuthService.refreshToken({ channelId })).twitchToken?.access_token || '';
      }

      const config = getCustomRewardsConfig(access_token, channelId);
      const rewards = await twitchRedemptionService.getCreatedRewards(access_token, channelId);
      let rewardId = rewards.find((id) => id === skipRewardId);

      if (!rewardId) {
        rewardId = await twitchRedemptionService.createReward({
          color: '#F57D07',
          cost: 25,
          title: 'Заказ видео'
        }, config);

        await PlayerModel.updateOne({ channelId }, { skipRewardId: rewardId });
      }

      res.json({ skipRewardId: rewardId, username, userId: channelId });
      next();
    } else {
      res.status(401);
      next();
    }
  })

  route.post('/auth', async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userData = await twitchAuthService.signIn(req.body.code);

      let token = jwt.sign(userData, config.JWT_SECRET, {
        expiresIn: 1000 * 60 * 60 * 24 * 30 * 12,
      });

      res.cookie('jwtToken', token);

      res.json({
        jwtToken: token,
      });

      next();
    } catch (e) {
      return next(new Error('Authentication failed'));
    }
  });

  route.delete('/reward', middlewares.TokenAuthMiddleware, async (req: Request, res: Response, next: NextFunction) => {
    const { channelId } = res.locals.userData;
    const user = await PlayerModel.findOne({ channelId }).exec();

    if (user && user.twitchToken?.access_token) {
      await twitchRedemptionService.closeRewards(user.twitchToken.access_token, channelId);
    }

    res.send();
    next();
  });

  route.get('/redemptions', async (req: Request, res: Response, next: NextFunction) => {
    const rewardId: any = req.query.rewardId;
    const username: any = req.query.username;
    const user = await PlayerModel.findOne({ username }).exec();

    if (user && user.twitchToken?.access_token) {
      const data = await twitchRedemptionService.getAllRedemptions(rewardId, RedemptionStatus.Unfulfilled, user.twitchToken.access_token, user.channelId);

      res.json(data);
    }

    next();
  });

  route.patch('/redemptions', async (req: Request, res: Response, next: NextFunction) => {
    const { redemptionId, username, status } = req.body;
    const user = await PlayerModel.findOne({ username }).exec();

    if (user && user.twitchToken?.access_token) {
      await twitchRedemptionService.setRedemptionStatus(user.twitchToken.access_token, user.channelId, redemptionId, user.skipRewardId, status);
    }

    res.send();
    next();
  });

  route.get('/refreshToken', async (req: Request, res: Response, next: NextFunction) => {
    const username: any = req.query.username;
    const { channelId, twitchToken } = await twitchAuthService.refreshToken({ username });

    res.json({ access_token: twitchToken?.access_token, channelId });
    next();
  });

  route.post('/skipEmotes', async (req, res, next) => {
    const { username, skipEmotes } = req.body;
    await PlayerModel.updateOne({ username }, { skipEmotes });

    res.send('OK');
    next();
  });

  route.get('/skipEmotes', async (req, res, next) => {
    const username: any = req.query.username;

    const user = await PlayerModel.findOne({ username }).exec();

    res.json(user?.skipEmotes);
    next();
  });

  route.post('/execCommand', async (req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'https://k4l75pkqk37niz2kioqeu4u2aym3s4.ext-twitch.tv');

    const { userId, command, channelId } = req.body;
    const { display_name: username } = await extensionCredentials.getUserData(userId);

    websocketServer.sendMessageByChannelId(channelId, {
      type: RESPOND_MESSAGE_TYPES.VIDEO_REQUEST_COMMAND,
      data: { username, command },
    });

    res.send('OK');
    next();
  })
}
