import { Router } from 'express';
import axios from 'axios';
import CONFIG from '../config';

const route = Router();

export default (app: Router) => {
  app.use('/random', route);

  route.get('/integer', async (req, res, next) => {
    const { min, max } = req.query as { min: string, max: string };

    if (min >= max) {
      res.send(min);
      next();
      return;
    }

    const params = { n: 1, min, max, apiKey: CONFIG.RANDOM_ORG_CLIENT_ID }
    try {
      const { data } = await axios.post('https://api.random.org/json-rpc/4/invoke', {
        jsonrpc: '2.0',
        method: 'generateIntegers',
        params,
        id: Math.random(),
      });

      res.send(data.result.random.data[0].toString());
      next();
    } catch (e) {
      res.status(500);
      next();
    }
  });
}
