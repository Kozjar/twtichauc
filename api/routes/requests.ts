import { Router } from 'express';
import axios from 'axios';
import { ENDPOINTS } from '../config/constants';

const route = Router();

export default (app: Router) => {
  app.use('/requests', route);

  route.get('/camilleBot', async (req, res, next) => {
    const { username, listKey } = req.query as { username: string, listKey: string };
    const { data } = await axios.get(`${ENDPOINTS.CAMILLE_BOT}/${username}/${listKey}/json`);

    res.send(JSON.stringify(data));
    next();
  })
}
