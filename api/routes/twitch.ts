import { NextFunction, Request, Response, Router } from 'express';
import jwt from 'jsonwebtoken';
import { Container } from 'typedi';
import config from '../config';
import { TwitchAuthService } from '../services/twitch-auth';
import UserService from '../services/user';
import TwitchRedemptionService from '../services/twitch-pub-sub/twitch-redemption-service';
import middlewares from '../middlewares';
import { RedemptionStatus } from '../config/constants';

const route = Router();

export default (app: Router) => {
    app.use('/twitch', route);

    const userService = Container.get(UserService);
    const twitchRedemptionService = Container.get(TwitchRedemptionService);

    route.post('/auth', async (req: Request, res: Response, next: NextFunction) => {
        try {
            const twitchAuthService = Container.get(TwitchAuthService);
            const userData = await twitchAuthService.signIn(req.body.code);

            let token = jwt.sign(userData, config.JWT_SECRET, {
                expiresIn: 1000 * 60 * 60 * 24 * 30 * 12,
            });

            res.cookie('jwtToken', token);
            res.json({
                jwtToken: token,
            });

            next();
        }
        catch(e) {
            return next(new Error('Authentication failed'));
        }
    });

    route.delete('/rewards', middlewares.TokenAuthMiddleware, async (req: Request, res: Response, next: NextFunction) => {
        const { channelId } = res.locals.userData;
        const user = await userService.findUser({ channelId });

        if (user && user.twitchToken?.access_token) {
            await twitchRedemptionService.closeRewards(user.twitchToken.access_token, channelId);
        }

        res.send();
        next();
    });

    route.get('/redemptions', async (req: Request, res: Response, next: NextFunction) => {
        const rewardId: any = req.query.rewardId;
        const username: any = req.query.username;
        const user = await userService.findUser({ username });

        if (user && user.twitchToken?.access_token) {
            const data = await twitchRedemptionService.getAllRedemptions(rewardId, RedemptionStatus.Unfulfilled, user.twitchToken.access_token, user.channelId);

            res.json(data);
        }

        next();
    });

    route.patch('/redemptions', async (req: Request, res: Response, next: NextFunction) => {
        const { rewardId, username, redemptionId, status } = req.body;
        const user = await userService.findUser({ username });

        if (user && user.twitchToken?.access_token) {
            await twitchRedemptionService.setRedemptionStatus(user.twitchToken.access_token, user.channelId, redemptionId, rewardId, status);
        }

        res.send();
        next();
    });

    route.get('/refreshToken', async (req: Request, res: Response, next: NextFunction) => {
        const twitchAuthService = Container.get(TwitchAuthService);
        const username: any = req.query.username;
        const { channelId, twitchToken} = await twitchAuthService.refreshToken({username});

        res.json({ access_token: twitchToken?.access_token, channelId });
        next();
    });
}