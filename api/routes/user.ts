import { Router } from 'express';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import UserService from '../services/user';
import user from '../models/user';

const route = Router();

export default (app: Router) => {
    app.use('/user', route);
    
    route.get('/userData', middlewares.TokenAuthMiddleware, async (req, res, next) => {
        const userService = Container.get(UserService);

        const user = await userService.findUser({ username: res.locals.userData!.username });

        if (user) {
            const { username, channelId: userId, settings, integration, daToken, twitchToken } = user;

            res.send({ userId, username, settings, integration, hasDAAuth: !!daToken?.access_token, hasTwitchAuth: !!twitchToken?.access_token });
        }
        next();
    });

    route.get('/token', async (req, res, next) => {
        const userService = Container.get(UserService);
        const username: any = req.query.username;

        const user = await userService.findUser({ username });

        if (user) {
            const { twitchToken, channelId } = user;
            res.send({ refresh_token: twitchToken?.refresh_token, channelId });
        } else {
            res.status(404).send('err');
        }

        next();
    });

    route.put('/token', async (req, res, next) => {
        const { username, twitchToken } = req.body;
        await user.updateOne({ username }, { twitchToken });

        res.send('OK');
        next();
    });
}
