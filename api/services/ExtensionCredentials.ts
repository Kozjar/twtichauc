import {
  Inject,
  Service
} from 'typedi';
import { TwitchAuthService } from './twitch-auth';
import axios from 'axios';
import config from '../config';

const clientParams = {
  client_id: config.TWITCH_CLIENT_ID,
  client_secret: config.TWITCH_CLIENT_SECRET,
}

@Service()
class ExtensionCredentials {
  token?: string;

  constructor(
    @Inject(() => TwitchAuthService) private twitchAuthService: TwitchAuthService,
  ) {
  }

  updateToken = async () => {
    const { access_token } = await this.twitchAuthService.getTwitchToken({ grant_type: 'client_credentials', ...clientParams });

    this.token = access_token;
  }

  getUserData = async (id: string) => {
    const { data: { data } } = await axios.get('https://api.twitch.tv/helix/users', {
      headers: {
        'Client-ID': clientParams.client_id,
        Authorization: `Bearer ${this.token}`,
      },
      params: { id },
    });

    return data[0];
  }
}

export default ExtensionCredentials;
