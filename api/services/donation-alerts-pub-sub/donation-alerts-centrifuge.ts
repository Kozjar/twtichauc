import { Service } from 'typedi';
import WebSocket from 'ws';
import Centrifuge from 'centrifuge';
import { IDonationAlertsCentrifugeDonationMessage } from './messages/donation-alerts-donation';
import Logger from "../logger";

interface DonationAlertsSubscribeCallbacksInterface {
    onDonation?: (message: IDonationAlertsCentrifugeDonationMessage) => void;
}

const DA_CENTRIFUGE_URL = 'wss://centrifugo.donationalerts.com/connection/websocket';
const DA_SUBSCRIBE_URL = 'https://www.donationalerts.com/api/v1/centrifuge/subscribe';
const RECONNECTION_DELAY_MIN = 1000;
const RECONNECTION_DELAY_MAX = 5000;

//@TODO Handle token refresh
@Service()
export default class DonationAlertsCentrifugeService extends Logger {
    private centrifugeClient?: Centrifuge;
    
    constructor(
        private daSocketConnectionToken: string,
        private subscribeCallbacks: DonationAlertsSubscribeCallbacksInterface,
        private userId: number,
    ) {
        super(`DonationAlertsCentrifugeService (${userId})`);
    }

    async connect(accessToken: string) {
        this.closeConnection();

        return new Promise((resolve, reject) => {
            this.centrifugeClient = new Centrifuge(DA_CENTRIFUGE_URL, {
                subscribeEndpoint: DA_SUBSCRIBE_URL,
                subscribeHeaders: {
                    Authorization: `Bearer ${accessToken}`
                },
                minRetry: RECONNECTION_DELAY_MIN,
                maxRetry: RECONNECTION_DELAY_MAX,
                websocket: WebSocket,
            });

            this.centrifugeClient.setToken(this.daSocketConnectionToken);

            this.centrifugeClient.on('connect', () => {
                this.log('connect');
            });
            this.centrifugeClient.on('disconnect', (err) => {
                this.log('disconnect');
                reject(err);
            });
            this.centrifugeClient.on('error', (err) => {
                this.log('error');
                reject(err);
            });

            this.centrifugeClient.connect();

            const subCallbacks = {
                publish: (message: any) => {
                    const messageType = message && message.data && message.data.message_type;
                    if (messageType && this.subscribeCallbacks.onDonation) {
                        this.subscribeCallbacks.onDonation(message as IDonationAlertsCentrifugeDonationMessage);
                    }
                },
                join: () => {
                    this.log('channel join');
                },
                subscribe: () => {
                    this.log('channel subscribe');
                    resolve(true);
                },
                error: (err: unknown) => {
                    this.log('channel subscribe error');
                    reject(err);
                },
                unsubscribe: () => {
                    this.log('channel unsubscribe');
                },
            };

            this.centrifugeClient.subscribe(`$alerts:donation_${this.userId}`, subCallbacks);
        });
    }

    closeConnection() {
        this.centrifugeClient?.disconnect();
    }
}