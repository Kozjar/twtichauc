class Logger {
  constructor(private readonly serviceName: string) {}

  log = (message: any): void => {
    if (typeof message === 'string') {
      console.log(`[${this.serviceName}]: ${message}`);
    } else {
      console.log(`[${this.serviceName}]:`);
      console.log(message);
    }
  };
}

export default Logger;
