import { Key } from "../types/common";
import Logger from './logger';

export enum AddUserStateEnum {
  Exist,
  First,
  Added,
}

export enum RemoveUserStateEnum {
  NotExist,
  Last,
  Removed,
}

class PubSubManager extends Logger {
  protected clients: Map<Key, string[]>;

  constructor(serviceName: string) {
    super(serviceName);

    this.clients = new Map<string, string[]>();
  }

  addUser = (channelId: Key, id: string): AddUserStateEnum => {
    const activeUsers = this.clients.get(channelId);

    if (!activeUsers) {
      this.clients.set(channelId, [id]);
      this.log(this.clients);

      return AddUserStateEnum.First;
    } else if (!activeUsers.includes(id)) {
      activeUsers.push(id);
      this.log(this.clients);

      return AddUserStateEnum.Added;
    }

    this.log(this.clients);
    return AddUserStateEnum.Exist;
  }

  removeUser = (channelId: Key, id: string): RemoveUserStateEnum => {
    const activeUsers = this.clients.get(channelId);

    if (!activeUsers?.includes(id)) {
      this.log(this.clients);
      return RemoveUserStateEnum.NotExist;
    }

    if (activeUsers?.length === 1) {
      this.clients.delete(channelId);
      this.log(this.clients);

      return RemoveUserStateEnum.Last;
    }

    const removeIndex = activeUsers.findIndex((client) => client === id);

    activeUsers.splice(removeIndex, 1);

    this.log(this.clients);
    return RemoveUserStateEnum.Removed;
  }
}

export default PubSubManager;
