import { Service } from 'typedi';
import { Telegraf } from 'telegraf';
import { IS_PRODUCTION } from '../config/constants';
import CONFIG from '../config';

@Service()
class TelegramBotService {
  // TODO: fix any type (fixed in new telegraf version, but build crushed with errors)
  private bot?: Telegraf<any>;

  constructor() {
  }

  connect = async (): Promise<void> => {
    if (!IS_PRODUCTION) {
      return;
    }

    this.bot = new Telegraf(CONFIG.TELEGRAM_BOT_TOKEN);
    await this.bot.launch();
  }

  logConnectedUser = async (username: string): Promise<void> => {
    await this.bot?.telegram.sendMessage('271681299', `connect ${username}`);
  }
}

export default TelegramBotService;
