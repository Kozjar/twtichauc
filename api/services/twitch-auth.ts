import { Inject, Service } from 'typedi';
import axios from 'axios';
import config from '../config';
import User, { ITwitchUser, IUser, UserTwitchToken } from '../models/user';
import PlayerModel from '../models/playerModel';
import { DEFAULT_USER } from '../types/common';
import { Document, Model } from 'mongoose';

interface ITwitchUserInfoResponse {
    token: {
        user_name: string;
        user_id: string;
    };
}

const clientParams = {
    client_id: config.TWITCH_CLIENT_ID,
    client_secret: config.TWITCH_CLIENT_SECRET,
}


class AuthService {
    constructor(
      private userModel: Model<ITwitchUser & Document>,
    ) {
    }

    public async signIn(code: string) {
        try {
            const twitchToken = await this.getTwitchToken({ code });
            const {
                token: {
                    user_name: username,
                    user_id: channelId,
                },
            } = await this.getUserInfo(twitchToken.access_token);
            const isRegistered = await this.userModel.exists({ channelId });
            const userData = { username, channelId };

            isRegistered
              ? await this.userModel.updateOne({ username }, { twitchToken })
              : await this.userModel.create({
                  ...userData,
                  twitchToken,
                  ...DEFAULT_USER,
              });

            return userData;
        }
        catch(e) {
            console.error(e);
            throw new Error('Failed to sign in');
        }
    }

    public async getUserInfo(token: string): Promise<ITwitchUserInfoResponse> {
        try {
            const { data } = await axios.get('https://api.twitch.tv/kraken', {
                headers: {
                    'Accept': 'application/vnd.twitchtv.v5+json',
                    'Client-ID': config.TWITCH_CLIENT_ID,
                    'Authorization': `OAuth ${token}`,
                }
            });

            return data;
        }
        catch(e) {
            console.error(e);
            throw e;
        }
    }

    public async getTwitchToken(params: any): Promise<UserTwitchToken> {
        try {
            const { data } = await axios.post('https://id.twitch.tv/oauth2/token', {}, {
                params: {
                    ...clientParams,
                    redirect_uri: config.TWITCH_REDIRECT_URL,
                    grant_type: 'authorization_code',
                    ...params,
                }
            });

            return data;
        }
        catch(e) {
            console.error(e);
            throw e;
        }
    }

    public async refreshToken(userQuery: Partial<IUser>): Promise<IUser> {
        const user = await this.userModel.findOne(userQuery);

        if (!user) {
            console.log(userQuery);
            throw new Error(`Couldn't find user`);
        }

        const { twitchToken, channelId } = user;
        const { data } = await axios.post<UserTwitchToken>('https://id.twitch.tv/oauth2/token', {}, {
            params: {
                ...clientParams,
                grant_type: 'refresh_token',
                refresh_token: twitchToken?.refresh_token,
            },
        });

        await this.userModel.updateOne({ channelId }, { twitchToken: data });

        return { ...user.toObject(), twitchToken: data };
    }

    validateToken = async (userQuery: Partial<IUser>): Promise<boolean> => {
        const user = await this.userModel.findOne(userQuery).exec();

        if (!user) {
            return false;
        }

        const { twitchToken }  = user;

        try {
            await axios.get('https://id.twitch.tv/oauth2/validate', { headers: { Authorization: `OAuth ${twitchToken?.access_token}` } });

            return true;
        } catch (e) {
            return false;
        }
    }
}

@Service()
export class TwitchAuthService extends AuthService {
    constructor(
        @Inject('userModel') userModel: typeof User,
    ) {
        super(userModel);
    }
}

@Service()
export class PlayerAuthService extends AuthService {
    constructor(
        @Inject('playerModel') userModel: typeof PlayerModel,
    ) {
        super(userModel);
    }
}
