import ws from 'ws';
import WebSocket from 'ws';
import config from '../../config';
import { TwitchAuthService } from '../twitch-auth';
import { TWITCH_TOPICS } from '../../config/constants';
import { ITwitchPubSubRedemptionMessage } from './messages/points-redemption';
import {
    REQUEST_MESSAGE_TYPE,
    TwitchPubSubRequestMessageInterface,
    TwitchPubSubResponseTypeMessageInterface
} from './messages/twitch-pub-sub';
import { Key } from '../../types/common';

enum MESSAGE_ERRORS {
    BAD_AUTH = 'ERR_BADAUTH',
    Forbidden = 'Forbidden',
}

interface ITwitchPubSubCallbacks {
    onRedemption?: (channelId: string, message: ITwitchPubSubRedemptionMessage) => void;
}

const PING_INTERVAL = 1000 * 10;

//@TODO Refactor to use single connection for all connected users
export default class TwitchPubSubService {
    ws?: WebSocket;
    pingHandle?: NodeJS.Timeout;

    constructor(
        private twitchAuthService: TwitchAuthService,
        private subscribeCallbacks: ITwitchPubSubCallbacks,
    ) {

    }

    private sendMessage(type: REQUEST_MESSAGE_TYPE, payload?: any) {
        let message: TwitchPubSubRequestMessageInterface = {
            type: type,
        };
        if (payload) {
            message.data = {
                ...payload,
            };
        }

        if (this.ws && this.ws.readyState === WebSocket.OPEN) {
            this.ws.send(JSON.stringify(message));
        }
    }

    async connect() {
        return new Promise(((resolve, reject) => {
            this.ws = new WebSocket(config.TWITCH_PUB_SUB_WS_URL);
            console.log('[TwitchPubSub] connecting');
            const resolveEmpty = () => resolve();
            this.ws.onopen = this.onOpen.bind(this, resolveEmpty);
            this.ws.onerror = this.onError;
            this.ws.onmessage = this.receiveMessage;
            this.ws.onclose = this.onClose.bind(this, reject);
        }));
    }

    listen = async (channelId: Key, accessToken: string, topic = TWITCH_TOPICS.REDEMPTIONS) => {
        console.log(`listen ${topic}.${channelId}`);

        if (!this.ws) {
            await this.connect();
        }
        
        this.sendMessage(REQUEST_MESSAGE_TYPE.LISTEN, {
            topics: [`${topic}.${channelId}`],
            auth_token: accessToken,
        });
    }

    unlisten = (channelId: Key, accessToken: string, topic = TWITCH_TOPICS.REDEMPTIONS) => {
        console.log(`unlisten ${topic}.${channelId}`);

        this.sendMessage(REQUEST_MESSAGE_TYPE.UNLISTEN, {
            topics: [`${topic}.${channelId}`],
            auth_token: accessToken,
        });
    }

    pingConnection = () => {
        console.log('ping');
        this.sendMessage(REQUEST_MESSAGE_TYPE.PING);
    }

    closeConnection = () => {
        if (this.ws) {
            this.ws.terminate();
            this.ws = undefined;
        }
    }

    onOpen = (resolve: () => void) => {
        console.log('opened');
        this.pingConnection();
        this.pingHandle = setInterval(this.pingConnection, PING_INTERVAL);
        resolve();
    }

    onError = (error: ws.ErrorEvent) => {
        console.log(error);
    }

    onClose = (reject: (reason?: any) => void) => {
        if (this.pingHandle) {
            clearInterval(this.pingHandle);
        }
        console.log('Twitch PubSub connection closed');
        reject();
    }

    receiveMessage = (event: WebSocket.MessageEvent) => {
        const twitchPubSubMessage = JSON.parse(event.data as string);
        const { data } = twitchPubSubMessage;
        console.log(`[PubSub Response Type] ${twitchPubSubMessage.type}`);

        if (twitchPubSubMessage.error === MESSAGE_ERRORS.BAD_AUTH) {
            console.log(twitchPubSubMessage);
            return;
        }
    
        if (!data) {
            return;
        }

        switch(twitchPubSubMessage.type) {
            case 'PONG': {
                //Not implemented
                //@TODO implement
                break;
            }
            
            case 'RECONNECT': {
                //Not implemented
                //@TODO implement
                break;
            }
            
            case "RESPONSE":
                //Not implemented
                //@TODO implement
                this.handleResponse(twitchPubSubMessage);
                break;

            case "MESSAGE":
                this.handleMessage(data.topic, JSON.parse(data.message));
                break;
        }
    }

    handleResponse = (message: TwitchPubSubResponseTypeMessageInterface) => {
        console.log('Response message:', message);
    }

    handleMessage = (topic: string, message: any) => {
        const [type, channelId] = topic.split('.');

        switch (type) {
            case TWITCH_TOPICS.REDEMPTIONS:
                if (this.subscribeCallbacks.onRedemption) {
                    this.subscribeCallbacks.onRedemption(channelId, message.data as ITwitchPubSubRedemptionMessage);
                }
            break;

            default:
                return;
        }
    }
}