import PubSubManager, { AddUserStateEnum, RemoveUserStateEnum } from '../pub-sub-manager';
import { Container, Inject, Service } from 'typedi';
import TwitchPubSubService from './twitch-pub-sub';
import TwitchRedemptionService from './twitch-redemption-service';
import User, { IUser } from '../../models/user';
import { TwitchAuthService } from '../twitch-auth';
import { ITwitchPubSubRedemptionMessage } from './messages/points-redemption';
import WebsocketServer from '../../websocket/websocket-server';
import ChannelPointsService from '../channel-points';
import { IS_PRODUCTION, RedemptionStatus } from '../../config/constants';

@Service()
class TwitchPubSubManager extends PubSubManager {
  private twitchPubSubService: TwitchPubSubService;

  constructor(
    @Inject('userModel') private userModel: typeof User,
    @Inject(() => WebsocketServer) private websocketServer: WebsocketServer,
    private twitchRedemptionService: TwitchRedemptionService,
  ) {
    super('TwitchPubSubManager');

    this.twitchPubSubService = new TwitchPubSubService(
      Container.get(TwitchAuthService),
      {
        onRedemption: this.handleRedemption,
      },
    );
  }

  handleRedemption = (channelId: string, message: ITwitchPubSubRedemptionMessage): void => {
    if (this.twitchRedemptionService.isAucRedemption(channelId, message.redemption)) {
      const ids = this.clients.get(channelId) || [];
      const purchase = ChannelPointsService.getPurchaseFromReward(message.redemption);

      this.websocketServer.sendMessageBySocketsId(ids, { type: 'PURCHASE', purchase });

      if (!IS_PRODUCTION) { // refund rewards while testing
        this.userModel.findOne({ channelId }).exec().then((user) => {
          if (user && purchase.rewardId && user.twitchToken?.access_token) {
            const { id, rewardId } = purchase;

            this.twitchRedemptionService.setRedemptionStatus(user.twitchToken.access_token, channelId, id, rewardId, RedemptionStatus.Canceled);
          }
        });
      }
    }
  }

  connectUser = async (
    {
      username,
      channelId,
      twitchToken,
      integration: { twitch: { rewards = [], rewardsPrefix = '' } },
    }: IUser,
    wsId: string,
  ): Promise<void> => {
    const addState = this.addUser(channelId, wsId);

    if (addState === AddUserStateEnum.Exist) {
      this.log(`${wsId} is already connected`);
      return;
    }

    if (addState === AddUserStateEnum.First && twitchToken?.access_token) {
      await Promise.all([
        this.twitchPubSubService.listen(channelId, twitchToken.access_token),
        this.twitchRedemptionService.openRewards(rewards, rewardsPrefix, twitchToken.access_token, channelId),
      ])
    }
  }

  disconnectUser = async ({ username, channelId, twitchToken }: IUser, wsId: string): Promise<void> => {
    const removeState = this.removeUser(channelId, wsId);

    if (!this.clients.size) { // disconnect from Twitch websocket if last user at all
      await this.twitchPubSubService.closeConnection();
    }

    if (removeState === RemoveUserStateEnum.NotExist) {
      this.log(`${wsId} is not connected`);
      return;
    }

    if (removeState === RemoveUserStateEnum.Last && twitchToken?.access_token) { // unlisten from topic and close rewards if last user with same id
      this.twitchPubSubService.unlisten(channelId, twitchToken.access_token);
      await this.twitchRedemptionService.hideRewards(twitchToken.access_token, channelId);
    }
  }
}

export default TwitchPubSubManager;
