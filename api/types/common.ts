import { IUser } from '../models/user';

export type Key = string | number;

export interface IPurchase {
  timestamp: string;
  cost: number;
  username: string;
  message: string;
  color: string;
  id: string;
  rewardId?: string;
  isDonation?: boolean;
}

export const DEFAULT_USER: Omit<IUser, 'username' | 'channelId' | 'twitchToken'> = {
  integration: {
    twitch: {
      isRefundAvailable: false,
      dynamicRewards: false,
      rewardsPrefix: 'ставка',
      rewards: [{ cost: 5000, color: '#e3924c' }],
    },
    da: {
      pointsRate: 100,
      isIncrementActive: false,
      incrementTime: 30,
    },
  },
  settings: {
    startTime: 10,
    isBuyoutVisible: true,
    timeStep: 60,
    isAutoincrementActive: false,
    autoincrementTime: 30,
    purchaseSort: 0,
    background: null,
  }
}