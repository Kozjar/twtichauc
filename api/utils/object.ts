import { Key } from '../types/common';

export type DotNotationObject = {
  [key: string]: Key | boolean;
}

const getPath = (key: string, parent?: string): string => parent ? `${parent}.${key}` : key;

export const objectToDotNotation = <T>(args: T, parent = '', result: DotNotationObject = {}): DotNotationObject =>
  Object.entries(args).reduce(
    (accum, [key, value]) =>
      value && typeof value === 'object' && !Array.isArray(value)
        ? objectToDotNotation(value, getPath(key, parent), accum)
        : {...accum, [getPath(key, parent)]: value},
    result,
  );
