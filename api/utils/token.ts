import { JwtTokenDataInterface } from '../interfaces/jwt-token-data';
import jwt from 'jsonwebtoken';
import config from '../config';

export const verifyToken = (token?: string): undefined | JwtTokenDataInterface => {
  if (!token) {
    return undefined
  }

  let result: undefined | JwtTokenDataInterface;

  try {
    jwt.verify(token, config.JWT_SECRET, (err, decoded) => {
      const { username, channelId } = decoded as JwtTokenDataInterface;
      return result = err ? undefined : { username, channelId };
    });
  } catch (e) {
    return undefined;
  }

  return result;
}
