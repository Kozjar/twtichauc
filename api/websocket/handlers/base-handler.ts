import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';

export default class BaseWsMessageHandler {
    constructor() {
    }

    handle(userConnectionId: string, userData: JwtTokenDataInterface, data?: any): void {
        throw new Error('Not implemented');
    }
}