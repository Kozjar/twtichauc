import BaseWsMessageHandler from './base-handler';
import Container from 'typedi';
import { TwitchAuthService } from '../../services/twitch-auth';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';
import { DonationAlertsAuthService } from '../../services/donation-alerts-auth';

//@TODO Remove
export default class IdentifyClientHandler extends BaseWsMessageHandler {
    static type = 'IDENTIFY_CLIENT';

    handle = async (userConnectionId: string, { channelId }: JwtTokenDataInterface) => {
        const twitchAuthService = Container.get(TwitchAuthService);
        const isValidTwitchToken = await twitchAuthService.validateToken({ channelId });
        const donationAlertsAuthService = Container.get(DonationAlertsAuthService);
        const isValidDaToken = await donationAlertsAuthService.validateToken(channelId);

        if (!isValidTwitchToken) {
            await twitchAuthService.refreshToken({ channelId });
        }

        if (!isValidDaToken) {
            await donationAlertsAuthService.refreshToken(channelId);
        }
    }
}