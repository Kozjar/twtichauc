import ChannelPointsService from '../../services/channel-points';
import BaseWsMessageHandler from './base-handler';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';
import Container from 'typedi';
import WebsocketServer from '../websocket-server';
import { RESPOND_MESSAGE_TYPES } from '../respond-message-types';
import { IPurchase } from '../../types/common';

export default class MockAuctionPurchaseHandler extends BaseWsMessageHandler {
    static type = 'GET_MOCK_DATA';

    async handle(wsId: string, userData: JwtTokenDataInterface, data?: Partial<IPurchase>) {
        const mockPurchase = ChannelPointsService.createMockPurchase(data);
        const websocketServer = Container.get(WebsocketServer);

        websocketServer.sendMessageBySocketsId([wsId], { type: RESPOND_MESSAGE_TYPES.PURCHASE, purchase: mockPurchase });
    }
}