import BaseWsMessageHandler from './base-handler';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';

export default class PingHandler extends BaseWsMessageHandler {
    static type = 'PING';

    async handle(userConnectionId: string, userData: JwtTokenDataInterface) {
        console.log('ping', userConnectionId);
    }
}