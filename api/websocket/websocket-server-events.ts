import { IWebsocketMessage } from "../interfaces/websocket-message";

export interface IWebsocketServerMessageEvent extends IWebsocketMessage {
    targets: string[];
}

export const WEBSOCKET_SERVER_EVENTS = {
    MESSAGE: 'MESSAGE',
};
